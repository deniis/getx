import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:state_mixin/controllers/myC.dart';

class HomePage extends GetView<MyC> {
  @override
  Widget build(BuildContext context) {
    final myC = Get.find<MyC>();
    return Scaffold(
      appBar: AppBar(title: Text("HOME PAGE")),
      body: Center(
        child: controller.obx(
          (state) => Text(state!),
          onLoading: Text("Loading..."),
        ),
      ),
      floatingActionButton:
          FloatingActionButton(onPressed: () => controller.getData()),
    );
  }
}
