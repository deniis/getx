import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:state_mixin/bindings/myB.dart';
import 'package:state_mixin/controllers/myC.dart';
import 'package:state_mixin/pages/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: HomePage(),
      initialBinding: MyB(),
    );
  }
}
