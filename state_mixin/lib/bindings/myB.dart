import 'package:get/get.dart';
import 'package:state_mixin/controllers/myC.dart';

class MyB extends Bindings {
  @override
  void dependencies() {
    Get.put(MyC());
  }
}
