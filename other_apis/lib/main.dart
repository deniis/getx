import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      body: Center(
          child: ElevatedButton(
        onPressed: () {
          Get.to(() => SecondPage(), arguments: 'Arg from home page');
        },
        child: Text("Second Page"),
      )),
    );
  }
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Page"),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text("${Get.arguments}"),
          Text(
            "${Get.rawRoute}",
            textAlign: TextAlign.center,
          ),
          Text(
            "${Get.routing.current}",
            textAlign: TextAlign.center,
          ),
          ElevatedButton(
            onPressed: () {
              Get.to(() => OtherPage());
            },
            child: Text("Other Page"),
          ),
        ],
      )),
    );
  }
}

class OtherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Other Page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text("Other Page"),
            Text("${Get.previousRoute}"),
            Text(
              "${Get.rawRoute}",
              textAlign: TextAlign.center,
            ),
            Text(
              "${GetPlatform.isIOS}",
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(onPressed: (){
        Get.until((route) => Get.currentRoute == '/');
      }),
    );
  }
}
