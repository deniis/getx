import 'package:get/get_navigation/get_navigation.dart';

class MyTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en': {
          'title': 'Hello @name Beli @things',
          'body': 'You have pushed the button this many times:'
        },
        'id': {
          'title': 'Halaman Utama',
          'body': 'Kamu telah menekan tombol sebanyak:'
        },
        'id_BT': {
          'title': 'Belah Depan',
          'body': 'Elu udah neken ni tombol:'
        }
      };
}
