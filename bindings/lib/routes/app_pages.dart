import 'package:bindings/bindings/countB.dart';
import 'package:bindings/controllers/countC.dart';
import 'package:bindings/pages/count.dart';
import 'package:bindings/pages/home.dart';
import 'package:bindings/routes/route_name.dart';
import 'package:get/get.dart';

class AppPage {
  static final pages = [
    GetPage(name: RouteName.home, page: () => HomePage()),
    GetPage(
        name: RouteName.count,
        page: () => CountPage(),
        binding: CountB())
  ];
}
