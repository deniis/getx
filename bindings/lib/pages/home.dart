import 'package:bindings/controllers/countC.dart';
import 'package:bindings/pages/count.dart';
import 'package:bindings/routes/route_name.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("HOME PAGE")),
      body: Center(
        child: ElevatedButton(
          onPressed: () => Get.toNamed(RouteName.count),
          child: Text(
            "NEXT >>",
          ),
        ),
      ),
    );
  }
}
