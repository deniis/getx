import 'package:bindings/controllers/countC.dart';
import 'package:bindings/pages/count.dart';
import 'package:bindings/pages/home.dart';
import 'package:bindings/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(home: HomePage(), getPages: AppPage.pages);
  }
}
