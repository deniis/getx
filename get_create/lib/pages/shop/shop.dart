import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_create/controllers/shopC.dart';
import 'package:get_create/pages/shop/widgets/shop_item.dart';

class ShopPage extends StatelessWidget {
  final shopC = Get.put(ShopC(), tag: 'total');
  final quantitiyC = Get.create(() => ShopC());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SHOP PAGE"),
      ),
      body: ListView.builder(
          itemCount: 10, itemBuilder: ((context, index) => ShopItem())),
      floatingActionButton:
          CircleAvatar(child: Obx(() => Text("${shopC.total}"))),
    );
  }
}
